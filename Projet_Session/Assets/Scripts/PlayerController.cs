﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public Sprite[] playerSprite;
    public float movementSpeed;
    public float movementSpeedModifier;
    public Vector2 velocity;
    //public Text t1;
    //public Text t2;

    private int stage;
    private int currentFrame;
    private int direction;
    private bool up, down, left, right, moved;
    private Rigidbody2D body;

    private int gametick = 0;


    void Start() {
        currentFrame = 0;
        stage = 0;
        direction = 0;
        movementSpeed = 100.0f;
        movementSpeedModifier = 1.0f;
        body = GetComponent<Rigidbody2D>();
    }


    void FixedUpdate() {
        body.MovePosition(body.position + velocity * Time.fixedDeltaTime);
    }


    void Update() {
        moved = false;

        //Getting Keyboard Inputs
        right = (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow));//(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow));
        left = (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow));//(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow));
        up = (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow));//(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow));
        down = (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow));//(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow));
        //movementSpeed = Input.GetAxis("Sprint") > 0 ? 60 : 30;


        //Setting Movement
        if (right && !left) {
            velocity.x = movementSpeed * Time.deltaTime;
            moved = true;
            direction = 6;
        }
        else if (left && !right) {
            velocity.x = -movementSpeed * Time.deltaTime;
            moved = true;
            direction = 2;
        }
        else
            velocity.x = 0;

        if (up && !down) {
            velocity.y = movementSpeed * Time.deltaTime;
            moved = true;
            if (left && !right)
                direction = 3;
            else if (!left && right)
                direction = 5;
            else
                direction = 4;
        }
        else if (down && !up) {
            velocity.y = -movementSpeed * Time.deltaTime;
            moved = true;
            if (left && !right)
                direction = 1;
            else if (!left && right)
                direction = 7;
            else
                direction = 0;
        }
        else
            velocity.y = 0;

        velocity *= movementSpeedModifier;

        //Animating
        if (moved) {
            /*if (Input.GetAxis("Sprint") > 0)
                currentFrame += 2;
            else*/
                currentFrame++;

            if (currentFrame >= 10) {
                currentFrame = 0;
                stage++;
                if (stage > 3)
                    stage = 0;
            }
        }
        else
            stage = 0;

        //t1.text = gametick.ToString();
        //gametick++;

        GetComponent<SpriteRenderer>().sprite = playerSprite[4 * direction + stage];
    }
}
