﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Main_Script : MonoBehaviour {
    void OnGUI() { if (Event.current.type.Equals(EventType.Repaint)) Render(); }

    public struct Cam {
        public int x;
        public int y;
        public int w;
        public int h;
    }

    private TileMap map;
    private CSprite player;
    private CSprite slime;
    public static CSprite Player;
    public static TileMap Map;
    public static Cam Main_Camera;
    private List<Slime> boids = new List<Slime>();
    private List<CSprite> DrawStack = new List<CSprite>();

    
    
    void Start() {
        map = new TileMap("Assets/Resources/Maps_Data/Mountain.txt");
        //map = new TileMap("Assets/Resources/Maps_Data/Overworld.txt");
        player = new Mario("mario", 8, 4);
        DrawStack.Add(player);

        Main_Script.Player = player;
        Main_Script.Map = map;
        Main_Script.Main_Camera = new Cam();
        Main_Script.Main_Camera.w = Screen.width;
        Main_Script.Main_Camera.h = Screen.height;

        DrawStack.Add(new Boulder(player, new Vector2(600, 500)));
        DrawStack.Add(new Boulder(player, new Vector2(1600, 750)));
        DrawStack.Add(new Boulder(player, new Vector2(700, 600)));
        DrawStack.Add(new Boulder(player, new Vector2(2200, 400)));
        DrawStack.Add(new Boulder(player, new Vector2(1800, 1000)));
        DrawStack.Add(new Boulder(player, new Vector2(700, 450)));


    }
	
	void Update () {

        Main_Script.Main_Camera.w = Screen.width;
        Main_Script.Main_Camera.h = Screen.height;

        if (boids.Count < 100) { 
            slime = new Slime("Slime", 4, 3);
            boids.Add(slime as Slime);
            (slime as Slime).boids = boids;
            DrawStack.Add(slime);
        }

        foreach (CSprite sprite in DrawStack) {
            sprite.Update();
        }

        for (int i = 0; i < DrawStack.Count - 1; i++) {
            if (DrawStack[i].Position.y > DrawStack[i + 1].Position.y) {
                CSprite temp = DrawStack[i];
                DrawStack[i] = DrawStack[i + 1];
                DrawStack[i + 1] = temp;
            }
        }
    }

    private void Render() {
        map.Render(player.Position);

        foreach( Slime slime in boids)
            slime.Render();

        foreach (CSprite sprite in DrawStack) {
            sprite.Render();
        }
    }
}