﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mario : CSprite {

	public Mario(string spriteSheet, int rows, int columns) : base(spriteSheet, rows, columns) {
        width = 31;
        height = 35;
        position = new Vector2(500, 600);
    }

    public override void Update() {

        bool right, left, up, down;
        //Vector2 lastPosition = new Vector2(position.x, position.y);
        Vector2 movement = new Vector2();

        right = (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow));
        left = (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow));
        up = (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow));
        down = (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow));

        if (up || down || left || right) {
            frames++;
            if (frames > 12) {
                frames = 0;
                state++;
                if (state > 3)
                    state = 0;
            }
        }
        else {
            frames = 11;
            state = 0;
        }

        if (right && !left) {
            movement.x += movementSpeed * Time.deltaTime;
            direction = 5;
        }
        else if (left && !right) {
            movement.x -= movementSpeed * Time.deltaTime;
            direction = 4;
        }
        if (up && !down) {
            movement.y -= movementSpeed * Time.deltaTime;
            switch (direction) {
                case 5: direction = 1; break;
                case 4: direction = 0; break;
                default: direction = 6; break;
            }
        }
        else if (down && !up) {
            movement.y += movementSpeed * Time.deltaTime;
            switch (direction) {
                case 5: direction = 3; break;
                case 4: direction = 2; break;
                default: direction = 7; break;
            }
        }

        if (Main_Script.Map.getTileIdAt((int)Mathf.Floor( (position.y + movement.y) / 64), (int)Mathf.Floor(position.x / 64)) == 85) {
            position.y += movement.y;
        }

        if (Main_Script.Map.getTileIdAt((int)Mathf.Floor(position.y / 64), (int)Mathf.Floor( (position.x + movement.x) / 64)) == 85) {
            position.x += movement.x;
        }
    }

    public override void Render() {
        

        int Nb_Rows = 9;
        int Nb_Columns = 16;

        float tileDim = 64.0f;//Main_Script.Main_Camera.w / Nb_Columns;
        //float ratio = tileDim / 64.0f;
        float x = position.x, y = position.y;
        if (position.x + Screen.width / 2 > 35 * 64.0f)
            x = (int)(position.x - 35 * 64.0f + Screen.width);
        else if (Screen.width / 2 < x)
            x = Screen.width / 2;

        if (position.y + Screen.height / 2 > 24 * 64.0f)
            y = (int)(position.y - 24 * 64.0f + Screen.height);
        else if (Screen.height / 2 < y)
            y = Screen.height / 2;

        GUI.DrawTexture(new Rect((int)x - 8, (int)y - 5, 16, 10), entityShadow);
        GUI.DrawTextureWithTexCoords(new Rect((int)x - width / 2, (int)y - height / 1.2f, width, height), sprite.Texture, sprite.GetRectFromId(4 * direction + state));
    }
}
