﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CSprite {

    protected TileSheet sprite;
    protected Texture entityShadow;

    protected int direction;
    protected int frames;
    protected int state;

    protected int width;
    protected int height;

    protected Vector2 position;

    public Vector2 Position {
        get { return position; }
    }

    protected Vector2 test = new Vector2();

    protected float movementSpeed;

    public CSprite() { }

    public CSprite(string spriteSheet, int rows, int columns) {
        sprite = new TileSheet(spriteSheet, rows, columns);
        //position = new Vector2( Screen.width / 2, Screen.height / 2);
        direction = 0;
        movementSpeed = 175.0f;
        entityShadow = TextureManager.GetTexture("EntityShadow");
    }

	public virtual void Update() {
    }

    public virtual void Render() {
    }
}
