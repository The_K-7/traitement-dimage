﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boulder : CSprite {

    Texture texture = TextureManager.GetTexture("pixel-boulders");//Resources.Load("pixel -boulders") as Texture;

    CSprite player;

    public Boulder(CSprite player, Vector2 pos) {
        this.player = player;
        position = pos;
        this.width = 50;
        this.height = 50;
    }

    public override void Render() {

        int offsetX = 0;// -(int)FocusPoint.x;
        int offsetY = 0;// -(int)FocusPoint.y;

        

        int Nb_Rows = 9;
        int Nb_Columns = 16;
        float tileDim = 64.0f;// Main_Script.Main_Camera.w / Nb_Columns;

        if ((int)player.Position.x + Screen.width / 2 > 35 * 64.0f)
            offsetX = (int)(Screen.width - 35 * 64.0f);
        else if (Screen.width / 2 < player.Position.x)
            offsetX = Screen.width / 2 - (int)player.Position.x;

        if ((int)player.Position.y + Screen.height / 2 > 24 * 64.0f)
            offsetY = (int)(Screen.height - 24 * 64.0f);
        else if (Screen.height/2 < player.Position.y)
            offsetY = Screen.height / 2 - (int)player.Position.y;


        GUI.DrawTextureWithTexCoords(new Rect(position.x + offsetX - width / 2, position.y + offsetY - height, width, height), texture, new Rect(0,0,1,1));
    }
}
