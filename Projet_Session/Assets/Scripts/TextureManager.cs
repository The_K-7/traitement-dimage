﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextureManager {

    private struct LoadedTexture {
        public LoadedTexture(string path, Texture texture) {
            this.path = path;
            this.texture = texture;
        }

        public Texture texture;
        public string path;
    }

    private static List<LoadedTexture> LoadedTextures = new List<LoadedTexture>();

	public static Texture GetTexture(string path) {
        foreach(LoadedTexture loaddedTexture in LoadedTextures) {
            if(loaddedTexture.path == path) {
                return loaddedTexture.texture;
            }
        }
        Texture texture = Resources.Load(path) as Texture;
        LoadedTextures.Add(new LoadedTexture(path, texture));
        return texture;
    }
}
