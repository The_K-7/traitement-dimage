﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Slime : CSprite {

    private Vector2 acceleration;
    private Vector2 velocity;
    private float mass = 1.0f;
    private float maxForce = 1.0f;
    private float maxSpeed = 1.0f;

    public float lifeTime;

    public List<Slime> boids;

    public Slime(string spriteSheet, int rows, int columns) : base(spriteSheet, rows, columns) {

        System.Random rand = new System.Random();

        this.width = 33;
        this.height = 25;
        this.direction = 3;
        this.position.x = rand.Next(600, 2000);
        this.position.y = rand.Next(400, 1300);
        this.lifeTime = rand.Next(16, 32);
        this.velocity = new Vector2(rand.Next(-10, 10) / 5, rand.Next(-10, 10) / 5);
        this.acceleration = new Vector2();
    }

    public void applyForce(Vector2 force) {
        Vector2 f = new Vector2(force.x, force.y) / mass;
        acceleration += f;
    }

    private void respawn() {
        System.Random rand = new System.Random();
        this.position.x = rand.Next(600, 2000);
        this.position.y = rand.Next(400, 1300);
        this.lifeTime = rand.Next(16, 32);
        velocity = new Vector2(rand.Next(-10, 10) / 5, rand.Next(-10, 10) / 5);
    }

    public override void Update() {

        lifeTime -= Time.deltaTime;

        if (lifeTime <= 0 || Vector2.Distance(position, Main_Script.Player.Position) < 20.0f) {
            respawn();
            return;
        }

        frames++;
        if (frames > 12) {
            frames = 0;
            state++;
            if (state > 2)
                state = 0;
        }

        System.Random rand = new System.Random();
        Vector2 movement = new Vector2();
        movement.x += rand.Next((int)(-movementSpeed),(int)(movementSpeed)) * Time.deltaTime;
        movement.y += rand.Next((int)(-movementSpeed), (int)(movementSpeed)) * Time.deltaTime;
        
        flock();

        velocity += acceleration;
        if (velocity.x > maxSpeed)
            velocity.x = maxSpeed;
        else if (velocity.x < -maxSpeed)
            velocity.x = -maxSpeed;

        if (velocity.y > maxSpeed)
            velocity.y = maxSpeed;
        else if (velocity.y < -maxSpeed)
            velocity.y = -maxSpeed;

        if (Main_Script.Map.getTileIdAt((int)Mathf.Floor((position.y + velocity.y + movement.y) / 64), (int)Mathf.Floor(position.x / 64)) == 85) {
            position.y += velocity.y + movement.y;
        }
        if (Main_Script.Map.getTileIdAt((int)Mathf.Floor(position.y / 64), (int)Mathf.Floor((position.x + velocity.x + movement.x) / 64)) == 85) {
            position.x += velocity.x + movement.x;
        }

        acceleration *= 0;
    }

    public override void Render() {

        CSprite player = Main_Script.Player;

        int offsetX = 0;
        int offsetY = 0;

        int Nb_Rows = 9;
        int Nb_Columns = 16;
        float tileDim = 64.0f;// Main_Script.Main_Camera.w / Nb_Columns;


        if ((int)player.Position.x + Screen.width / 2 > 35 * 64.0f)
            offsetX = (int)(Screen.width - 35 * 64.0f);
        else if (Screen.width / 2 < player.Position.x)
            offsetX = Screen.width / 2 - (int)player.Position.x;

        if ((int)player.Position.y + Screen.height / 2 > 24 * 64.0f)
            offsetY = (int)(Screen.height - 24 * 64.0f);
        else if (Screen.height / 2 < player.Position.y)
            offsetY = Screen.height / 2 - (int)player.Position.y;

        /*
        if (player.Position.y + Screen.height / 2 > 24 * 64.0f)
            offsetY = (int)(Screen.height * 1.5 - 24 * 64.0f);
        else if (Screen.height / 2 < player.Position.y)
            offsetY = Screen.width / 2 - (int)player.Position.y;*/

        int x = (int)position.x + offsetX;
        int y = (int)position.y + offsetY;

        GUI.DrawTexture(new Rect(x - 8, y - 5, 16, 10), entityShadow);
        GUI.DrawTextureWithTexCoords(new Rect(x - width / 2, y - height / 1.2f, width, height), sprite.Texture, sprite.GetRectFromId(3 * direction + state));
    }


    private void flock() {
        Vector2 sep = separate();
        Vector2 ali = align();
        Vector2 coh = cohesion();
        
        sep *= 1.5f;
        ali *= 1.0f;
        coh *= 1.0f;

        applyForce(sep);
        applyForce(ali);
        //applyForce(coh);
    }

    private Vector2 separate() {
        float desiredSeparation = 25.0f;
        Vector2 steer = new Vector2();
        int count = 0;

        float distance;
        foreach (Slime boid in boids) {
            distance = Vector2.Distance(position, boid.position);
            if (distance > 0 && distance < desiredSeparation) {
                Vector2 diff = position - boid.position;
                diff = diff.normalized;
                diff /= distance;
                steer += diff;
                count++;
            }
        }

        if (count > 0)
            steer /= count;

        if (steer.sqrMagnitude > 0) {
            steer = steer.normalized;
            steer -= velocity;

            if (steer.x > maxForce)
                steer.x = maxForce;
            else if (steer.x < -maxForce)
                steer.x = -maxForce;

            if (steer.y > maxForce)
                steer.y = maxForce;
            else if (steer.y < -maxForce)
                steer.y = -maxForce;

            //steer.limit(maxForce);
        }

        return steer;
    }

    private Vector2 align() {
        float neightborDistance = 50.0f;
        Vector2 sum = new Vector2(0, 0);
        int count = 0;

        float distance;
        foreach (Slime boid in boids) {
            distance = Vector2.Distance(position, boid.position);

            if (distance > 0 && distance < neightborDistance) {
                sum += boid.velocity;
                count++;
            }
        }

        Vector2 steer;
        if (count > 0) {
            sum /= count;
            sum = sum.normalized;
            //sum.mult(maxSpeed);
            steer = sum - velocity;
            //steer.limit(maxForce);
        }
        else
            steer = new Vector2(0, 0);

        return steer;
    }

    private Vector2 seek(Vector2 target) {
        Vector2 desired = target - position;
        desired = desired.normalized;
        //desired.normalize();
        //Calculating the desired velocity to target at max speed
        //desired.mult(maxspeed);

        //Reynolds’s formula for steering force

        Vector2 steer = desired - velocity;
        //Using our physics model and applying the force to the object’s acceleration
        return steer;
    }

    private Vector2 cohesion() {
        float neightborDistance = 50.0f;
        Vector2 sum = new Vector2(0, 0);
        int count = 0;

        float distance;
        foreach (Slime boid in boids) {
            distance = Vector2.Distance(position, boid.position);

            if (distance > 0 && distance < neightborDistance) {
                sum += boid.position;
                count++;
            }
        }

        if (count > 0) {
            sum /= count;
            return seek(sum);
        }

        return new Vector2(0, 0);
    }

}
