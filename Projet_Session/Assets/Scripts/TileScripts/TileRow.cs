﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileRow {

    private List<Tile> tiles;

	public TileRow() {

        tiles = new List<Tile>();

    }

    public void AddToRow(int id) {
        tiles.Add(new Tile(id));
    }

    public List<Tile> GetTiles() {
        return tiles;
    }
}
