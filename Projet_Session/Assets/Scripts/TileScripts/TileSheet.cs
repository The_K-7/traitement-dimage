﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSheet {

    private struct Vec2 {
        public float X;
        public float Y;
        public Vec2(float X, float Y) {
            this.X = X;
            this.Y = Y;
        }
    }

    private Vec2[] cache;
    private Texture _texture;
    private float width;
    private float height;
    
    public Texture Texture {
        get { return _texture; }
    }

    public TileSheet(string textureName, int columns_X, int rows_Y) {
        _texture = TextureManager.GetTexture(textureName);

        cache = new Vec2[columns_X * rows_Y];

        width = 1.0f / columns_X;
        height = 1.0f / rows_Y;

        int id = 0;
        for (int x = 0; x < columns_X; x++)
            for (int y = 0; y < rows_Y; y++) {
                cache[id] = new Vec2((float)(id % rows_Y) / rows_Y, (float)(id / rows_Y) / columns_X);
                id++;
            }
    }

    public Rect GetRectFromId(int id) {
        return new Rect(cache[id].X, cache[id].Y, height, width);
    }
}
