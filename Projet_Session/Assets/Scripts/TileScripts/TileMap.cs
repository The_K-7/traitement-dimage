﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TileMap {
    
    private TileSheet tileSheet;
    private List<TileRow> rows;

    private int number_row;
    private int number_column;
    private int tileDim = 64;

    public TileMap(string mapName) {

        if (!File.Exists(mapName))
            throw new System.Exception("Map data doesn't exist.");

        rows = new List<TileRow>();

        StreamReader file = new StreamReader(mapName);

        string line = file.ReadLine();
        string[] parts = line.Split(' ');
        
        tileSheet = new TileSheet(parts[0], int.Parse(parts[1]), int.Parse(parts[2]));
        
        TileRow row;
        while ((line = file.ReadLine()) != null) {
            row = new TileRow();
            parts = line.Split(' ');

            if (parts.Length > number_column)
                number_column = parts.Length;

            for (int i = 0; i < parts.Length; i++)
                row.AddToRow(int.Parse(parts[i]));

            rows.Add(row);
            number_row++;
        }

        file.Close();
    }

    public int getTileIdAt(int row, int column) {
        if (rows.Count > row) {
            List<Tile> tiles = rows[row].GetTiles();
            if (tiles.Count > column)
                return tiles[column].TileId;
        }
        return -1;
    }

    public void Render(Vector2 FocusPoint) {
        int y = 0, x = 0;

        int offsetX = 0;
        int offsetY = 0;

        if (FocusPoint.x + Screen.width / 2 > number_column * 64.0f)
            offsetX = (int)(Screen.width - number_column * 64.0f);
        else if (Screen.width / 2 < FocusPoint.x)
            offsetX = (int)(Screen.width / 2 - FocusPoint.x);

        if (FocusPoint.y + Screen.height / 2 > number_row * 64.0f)
            offsetY = (int)(Screen.height - number_row * 64.0f);
        else if (Screen.height / 2 < FocusPoint.y)
            offsetY = (int)(Screen.height / 2 - FocusPoint.y);
        


        float camX = -(float)offsetX / 150;
        float camY = (float)offsetY / 500;

        if (camY < -2.66f) camY = -2.66f;
        Camera.main.transform.position = new Vector3(camX, camY, -10);

        int posY;
        int posX;
        foreach (TileRow row in rows) {
            posY = offsetY + y * tileDim;
            if (posY + tileDim >= camY && posY - tileDim <= camY + Screen.height) {
                foreach (Tile tile in row.GetTiles()) {
                    posX = offsetX + x * tileDim;
                    if (posX + tileDim + 10 >= camX && posX - tileDim <= camX + Screen.width)
                        GUI.DrawTextureWithTexCoords(new Rect(posX, posY, tileDim, tileDim), tileSheet.Texture, tileSheet.GetRectFromId(tile.TileId));
                    ++x;
                }
            }
            ++y;
            x = 0;
        }
    }
}