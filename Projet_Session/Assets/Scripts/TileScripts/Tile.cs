﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile {

    private int tileId;

    public int TileId {
        get { return tileId; }
    }

	public Tile(int id) {
        tileId = id;
    }

}
